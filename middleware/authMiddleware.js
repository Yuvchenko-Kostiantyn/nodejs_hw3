const jwt = require('jsonwebtoken');
const config = require('config');
const {secret} = config;

module.exports.authorize = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  if (!authHeader) {
    throw {status: 401, message: 'Unauthorized user'};
  }

  try {
    req.user = jwt.verify(authHeader, secret);
    next();
  } catch (err) {
    next(err);
  }
};

module.exports.authorizeDriver = (req, res, next) => {
  const {role} = req.user;

  if (role !== 'DRIVER') {
    throw {status: 403, message: 'Forbidden'};
  }

  try {
    next();
  } catch (err) {
    next(err);
  }
};
