const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const cors = require('cors');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');

const dbconfig = config.get('dbconfig');
const {PORT} = config;
const {DB_USER, DB_PASSWORD, DB_NAME} = dbconfig;

const {errorHandler} = require('./middleware/errorHandler');
const {authorize, authorizeDriver} = require('./middleware/authMiddleware');

const app = express();

mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASSWORD}@main-cluster.qnnvg.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
}).then(() => console.log('DB connected'));

const authRouter = require('./routers/authRouter');
const loadRouter = require('./routers/loadRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/trucksRouter');

app.use(cors());
app.use(express.json());
app.use(morgan('common', {
  stream: fs.createWriteStream(
      path.join(__dirname, 'access.log'), {flags: 'a'}),
}));

app.use('/api/auth', authRouter);

app.use(authorize);
app.use('/api/users/me', userRouter);
app.use('/api/loads', loadRouter);

app.use(authorizeDriver);
app.use('/api/trucks', truckRouter);

app.use(errorHandler);

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
