const express = require('express');
const router = express.Router();
const {
  register,
  login,
  recoverPassword} = require('../controllers/authController');

router.post('/register', register);
router.post('/login', login);
router.post('/forgot_password', recoverPassword);

module.exports = router;
