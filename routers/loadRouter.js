const express = require('express');
const router = express.Router();

const {
  getLoads,
  getActiveLoads,
  deleteLoadById,
  getLoadById,
  create,
  postLoad,
  iterateActiveState,
  updateLoad,
  getShippingInfo} = require('../controllers/loadController');

router.get('/', getLoads);
router.post('/', create);
router.get('/active', getActiveLoads);
router.patch('/active/state', iterateActiveState);
router.get('/:id', getLoadById);
router.put('/:id', updateLoad);
router.delete('/:id', deleteLoadById);
router.post('/:id/post', postLoad);
router.get('/:id/shipping_info', getShippingInfo);

module.exports = router;
