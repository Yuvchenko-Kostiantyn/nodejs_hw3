const express = require('express');
const router = express.Router();

const {
  getUser,
  deleteUser,
  changePassword} = require('../controllers/userController');

router.get('/', getUser);
router.delete('/', deleteUser);
router.patch('/password', changePassword);

module.exports = router;
