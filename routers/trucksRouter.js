const express = require('express');
const router = express.Router();

const {
  getTrucks,
  addTruck,
  getTruckById,
  deleteTruck,
  assignTruck,
  editTruck} = require('../controllers/trucksController');

router.get('/', getTrucks);
router.post('/', addTruck);
router.get('/:id', getTruckById);
router.put('/:id', editTruck);
router.delete('/:id', deleteTruck);
router.post('/:id/assign', assignTruck);

module.exports = router;
