const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('load', new Schema({
  created_by: String,
  assigned_to: String,
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: [
      'On Stock',
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery',
    ],
    default: 'On Stock',
  },
  name: String,
  payload: Number,
  pickup_address: String,
  delivery_address: String,
  logs: [
    {message: String, time: {type: Date, default: new Date()}, _id: false},
  ],
  dimensions: {
    width: Number,
    length: Number,
    height: Number,
  },
  created_date: {
    type: Date,
    default: new Date(),
  },
}));
