const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('truck', new Schema({
  created_by: String,
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: [null, 'OL', 'IS'],
    default: null,
  },
  created_date: {
    type: Date,
    default: new Date(),
  },
}));
