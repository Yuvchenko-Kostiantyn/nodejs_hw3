const mongoose = require('mongoose');
const validator = require('validator');
const Schema = mongoose.Schema;

module.exports = mongoose.model('user', new Schema({
  email: {
    required: true,
    type: String,
    unique: true,
    validate: (value) => {
      return validator.isEmail(value);
    },
  },
  password: {
    required: true,
    type: String,
  },
  role: {
    required: true,
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
  },
  createdDate: {
    type: Date,
    default: new Date(),
  },
}));
