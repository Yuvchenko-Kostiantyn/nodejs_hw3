const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('config');
const mailgun = require('mailgun-js');
const passGen = require('password-generator');
const {secret} = config;
const {API_KEY, DOMAIN} = config.get('mailgunConfig');
const mg = mailgun({apiKey: API_KEY, domain: DOMAIN});

module.exports.register = async (req, res, next) => {
  const {email, password, role} = req.body;
  const hashedPass = await bcrypt.hash(password, 10);
  try {
    const user = new User({email, password: hashedPass, role});
    await user.save();
    res.status(200).json({message: 'Success'});
  } catch (err) {
    next(err);
  }
};

module.exports.login = async (req, res, next) => {
  const {email, password} = req.body;
  try {
    const user = await User.findOne({email}).exec();
    if (!user) throw {message: 'Invalid login'};

    const isValid = await bcrypt.compare(password, user.password);

    if (!isValid) throw {status: 400, message: 'Invalid password'};

    const userData = {
      id: user._id,
      email: user.email,
      role: user.role,
      createdDate: user.createdDate,
    };

    res.status(200).json({status: 'ok', jwt_token: jwt.sign(JSON.stringify(userData), secret)});
  } catch (err) {
    next(err);
  }
};

module.exports.recoverPassword = async (req, res, next) => {
  try {
    const user = await User.findOne({email: req.body.email});
    if (!user) {
      throw {status: 400, message: 'No user with this email found'};
    }
    const newPass = passGen(16, false);
    const newHash = await bcrypt.hash(newPass, 10);
    user.password = newHash;
    await user.save();
    const data = {
      from: 'Mailgun Sandbox <postmaster@sandbox7abf47f1ada54d6896de23c25e28a83d.mailgun.org>',
      to: req.body.email,
      subject: 'Your new password',
      text: `Your new pasword is ${newPass}`,
    };
    await mg.messages().send(data);
    res.status(200).json('New password sent to your email address');
  } catch (err) {
    next(err);
  }
};
