const Truck = require('../models/truck');

module.exports.getTrucks = async (req, res, next) => {
  const {id} = req.user;
  try {
    const trucks = await Truck.find({created_by: id});
    res.status(200).json(trucks);
  } catch (err) {
    next(err);
  }
};

module.exports.getTruckById = async (req, res, next) => {
  try {
    const truck = await Truck.findById(req.params.id);

    if (!truck) {
      throw ({status: 400, message: 'No truck with given id found'});
    };

    res.status(200).json(truck);
  } catch (err) {
    if (err.kind === 'ObjectId') {
      err.status = 400;
    }
    console.log(err);
    next(err);
  }
};

module.exports.addTruck = async (req, res, next) => {
  const {id} = req.user;
  const {type} = req.body;
  try {
    const truck = new Truck({created_by: id, type});
    await truck.save();
    res.status(200).json({message: 'Truck added successfully'});
  } catch (err) {
    next(err);
  }
};

module.exports.editTruck = async (req, res, next) => {
  try {
    await Truck.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    next(err);
  }
};

module.exports.deleteTruck = async (req, res, next) => {
  try {
    await Truck.findByIdAndDelete(req.params.id);
    res.status(200).json({message: 'Truck deleted successfully'});
  } catch (err) {
    next(err);
  }
};

module.exports.assignTruck = async (req, res, next) => {
  const {id} = req.user;
  try {
    const assignedTruck = await Truck.findOne({assigned_to: id});
    if (assignedTruck) {
      if (assignedTruck.status === 'OL') {
        throw {status: 404, message: 'You can\'t assign truck while on load'};
      }

      assignedTruck.assigned_to = null;
      assignedTruck.status = null;
      await assignedTruck.save();
    }
    await Truck.findByIdAndUpdate(req.params.id, {assigned_to: id, status: 'IS'});
    res.status(200).json({message: 'Success'});
  } catch (err) {
    next(err);
  };
};
