const User = require('../models/user');
const bcrypt = require('bcrypt');

module.exports.getUser = (req, res, next) => {
  try {
    res.json(req.user);
  } catch (err) {
    next({message: err.message});
  }
};

module.exports.deleteUser = async (req, res, next) => {
  try {
    await User.findByIdAndDelete(req.user.id);
    res.json({status: 'Success'});
  } catch (err) {
    next({status: 400, message: err.message});
  }
};

module.exports.changePassword = async (req, res, next) => {
  const {newPassword} = req.body;
  try {
    const user = await User.findById(req.user.id);
    const isPassTheSame = await bcrypt.compare(newPassword, user.password);
    if (isPassTheSame) {
      throw {status: 400, message: 'New password is the same as the old one'};
    }
    const newHash = await bcrypt.hash(newPassword, 10);
    user.password = newHash;
    await user.save();
    res.status(200).json({message: 'Success'});
  } catch (err) {
    next(err);
  }
};
