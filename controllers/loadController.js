const Load = require('../models/load');
const Truck = require('../models/truck');
const config = require('config');
const {loadStates, truckTypes} = config;

module.exports.getLoads = async (req, res, next) => {
  const {id} = req.user;

  try {
    const loads = await Load.find({$or: [{created_by: id}, {assigned_to: id}]});
    res.json(loads);
  } catch (err) {
    next(err);
  }
};

module.exports.getLoadById = async (req, res, next) => {
  try {
    const load = await Load.findOne({_id: req.params.id, status: {$in: ['NEW', 'POSTED']}});
    res.status(200).json(load);
  } catch (err) {
    next(err);
  }
};

module.exports.deleteLoadById = async (req, res, next) => {
  try {
    await Load.findByIdAndDelete(req.params.id);
    res.status(200).json({message: 'Load deleted successfully'});
  } catch (err) {
    next(err);
  }
};

module.exports.getActiveLoads= async (req, res, next) => {
  const {id, role} = req.user;
  try {
    if (role === 'DRIVER') {
      const loads = await Load.find({assigned_to: id}, {status: 'ASSIGNED'});
      res.json(loads);
    } else {
      throw {status: 403, message: 'Forbidden'};
    }
  } catch (err) {
    next(err);
  }
};

module.exports.iterateActiveState = async (req, res, next) => {
  try {
    const load = await Load.findOne({assigned_to: req.user.id, status: 'ASSIGNED'});
    const truck = await Truck.findOne({assigned_to: req.user.id, status: 'OL'});
    if (!load) {
      throw {status: 400, message: 'You have no active loads at this moment'};
    }
    const currentIndex = loadStates.indexOf(load.state);
    if (load.state === loadStates[loadStates.length-2]) {
      load.state = loadStates[loadStates.currentIndex+1];
      load.status = 'SHIPPED';
      load.logs.push(`Load state changed to ${loadStates[currentIndex+1]} by driver ${load.assigned_to}`);
      load.logs.push(`Load status changed to SHIPPED by driver ${load.assigned_to}`);
      truck.status = 'IS';
    } else {
      load.state = loadStates[currentIndex+1];
      load.logs.push(`Load state changed to ${loadStates[currentIndex+1]} by driver ${load.assigned_to}`);
    }

    await load.save();
    await truck.save();

    res.status(200).json(load);
  } catch (err) {
    next(err);
  }
};

module.exports.create = async (req, res, next) => {
  const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
  const id = req.user.id;
  const load = new Load({created_by: id, name, payload, pickup_address, delivery_address, dimensions});
  try {
    await load.save();
    res.status(200).json({message: 'Success'});
  } catch (err) {
    next(err);
  }
};

module.exports.updateLoad = async (req, res, next) => {
  try {
    if (req.user.role !== 'SHIPPER') {
      throw {status: 403, message: 'Forbidden'};
    }
    const update = await Load.findByIdAndUpdate(req.params.id, req.body);
    res.status(200).json(update);
  } catch (err) {
    next(err);
  }
};

module.exports.postLoad = async (req, res, next) => {
  if (req.user.role !== 'SHIPPER') {
    throw {status: 403, message: 'Forbidden'};
  }

  try {
    const load = await Load.findById(req.params.id);
    load.status = 'POSTED';
    await load.save();
    const compatibleType = truckTypes.find((el) => {
      el.max_weight >= load.payload &&
      el.width >= load.dimensions.width &&
      el.height >= load.dimensions.height &&
      el.length >= load.dimensions.length;
    });
    const availableTrucks = await Truck.find({assigned_to: {$ne: null}, status: 'IS', type: compatibleType.type});

    if (availableTrucks.length<1) {
      throw {status: 200, message: 'No suitable truck found'};
    }

    const chosenTruck = availableTrucks[0];
    const chosenDriver = chosenTruck.assigned_to;

    chosenTruck.status = 'OL';

    load.assigned_to = chosenDriver;
    load.status = 'ASSIGNED';
    load.state = 'En route to Pick Up';
    load.logs.push({message: `Load assigned to driver with id ${chosenDriver}`});
    await load.save();
    await chosenTruck.save();

    res.status(200).json({message: 'Load posted successfully', driver_found: true});
  } catch (err) {
    next(err);
  }
};

module.exports.getShippingInfo = async (req, res, next) => {
  try {
    if (req.user.role !== 'SHIPPER') {
      throw {status: 403, message: 'Forbidden'};
    }

    const load = await Load.findOne({_id: req.params.id, status: 'ASSIGNED'});
    const truck = await Truck.findOne({assigned_to: load.assigned_to});
    res.status(200).json({load, truck});
  } catch (err) {
    next(err);
  }
};
